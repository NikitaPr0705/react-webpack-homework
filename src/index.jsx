import React from 'react';
import ReactDOM from 'react-dom';


import { App } from './App'
import './styles/main.scss'
// import './assets/Arial/ArialRegular/ArialRegular.ttf'
// import './assets/Arial/ArialBold/ArialBold.ttf'



ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);


