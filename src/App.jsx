import React from 'react';

import ProductName from "./components/ProductName";
import AboutProduct from "./components/AboutProduct";
import ProductFeatures from "./components/ProductFeatures";
import ProductScreenshots from "./components/ProductScreenshots";
import ProductReviews from "./components/ProductReviews";
import BuyItNow from "./components/BuyItNow";
import Contacts from "./components/Contacts";
import Footer from "./components/Footer";



export function App() {
    return (
        <div className="App">
                <ProductName />
                <AboutProduct/>
                <ProductFeatures />
                <ProductScreenshots />
                <ProductReviews />
                <BuyItNow />
                <Contacts />
                <Footer />
        </div>
    );
}
