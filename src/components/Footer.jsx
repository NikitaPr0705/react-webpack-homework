import React from 'react';

function Footer() {
    return (
        <div className='footer'>
            <div className='footer-wrapper'>
                <div className='content-wrapper'>
                <span className='footer-text'>Copyright © 2014 Product name · PSD HTML CSS</span>
            </div>
            </div>
        </div>
    )
}

export default Footer;