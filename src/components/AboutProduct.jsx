import React from 'react';

function AboutProduct() {
    return (
        <div className='about-product'>
            <div className='content-wrapper'>
                <div className='about-product__text-wrapper'>
                <h2 className='about-product__title title-h2'>About your product</h2>
                <p className='about-product__text'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga, illo at. Natus eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam sapiente ut ipsum.</p>
                </div>
                <div className='about-product__image-wrapper'>
                    <div className='about-product__image'/>
                </div>
            </div>
        </div>
    )
}

export default AboutProduct;