import React from 'react';
import ProductReviewsItem from "./ProductReviewsItem";

function ProductReviews() {
    return (
        <div className='product-reviews'>
            <div className='product-reviews-wrapper content-wrapper'>
                <h2 className='title-h2'>Reviews</h2>
                <ul className='product-reviews__list'>
                    <ProductReviewsItem />
                </ul>
            </div>
        </div>
    )
}

export default ProductReviews;