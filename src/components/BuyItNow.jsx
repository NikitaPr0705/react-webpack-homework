// import React from 'react';
//
// function BuyItNow() {
//     return (
//         <div>BuyItNow</div>
//     )
// }
//
// export default BuyItNow;


import React from 'react';
import BuyItNowItem from "./BuyItNowItem";

function BuyItNow() {
    return (
        <div className='buy-it-now'>
            <div className='buy-it-now-wrapper content-wrapper'>
                <h2 className='title-h2'>Buy It Now</h2>
                <ul className='buy-it-now__list'>
                    <BuyItNowItem />
                </ul>
            </div>
        </div>
    )
}

export default BuyItNow;