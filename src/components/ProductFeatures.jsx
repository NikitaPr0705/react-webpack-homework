import React from 'react';
import ProductFeaturesItem from "./productFeaturesItem";

function ProductFeatures() {
    return (
        <div className='product-features'>
            <div className='product-features-wrapper content-wrapper'>
                <h2 className='title-h2'>Dignity and pluses product</h2>
                <ul className='product-features__list'>
                    <ProductFeaturesItem/>
                </ul>
            </div>
        </div>
    )
}

export default ProductFeatures;