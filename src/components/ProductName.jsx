import React from 'react';

function ProductName() {
    return (
        <div className='product-name'>
            <div className='product-name-wrapper content-wrapper'>
                <div className='product-name__text-wrapper'>
                <h2 className='product-name__title'>Product Name</h2>
                <ul className='product-name__list'>
                    <li className='product-name__item'>Put on this page information about your product
                    </li>
                    <li className='product-name__item'>A detailed description of your product
                    </li>
                    <li className='product-name__item'>Tell us about the advantages and merits</li>
                    <li className='product-name__item'>Associate the page with the payment system
                    </li>
                </ul>
                </div>
                <div className='product-name__image-wrapper'>
                    <div className='product-name__image'></div>
                </div>
            </div>

        </div>
    )
}

export default ProductName;
