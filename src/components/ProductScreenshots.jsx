import React from 'react';
import ProductScreenshotsItem from "./ProductScreenshotsItem";

function ProductScreenshots() {
    return (
        <div className='product-screenshots'>
            <div className='product-screenshots-wrapper content-wrapper'>
                <h2 className='title-h2'>Screenshots</h2>
                <ul className='product-screenshots__list'>
                    <ProductScreenshotsItem />
                </ul>
            </div>
        </div>
    )
}

export default ProductScreenshots;