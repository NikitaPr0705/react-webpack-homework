import React from 'react';

function ProductFeaturesItem() {
    return (
        <li>Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam, dolore minus inventore.</li>
    )
}

export default ProductFeaturesItem;