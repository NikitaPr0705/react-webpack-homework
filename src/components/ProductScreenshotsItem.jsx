import React from 'react';

function ProductScreenshotsItem() {
    return (
        <li className='product-screenshots__item'>
            <div className='product-screenshots__image'></div>
            <div className='product-screenshots__text-wrapper'>
                <h3 className='product-screenshots__item-title'>The description for the image</h3>
                <p className='product-screenshots__item-text'>
                    Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde perferendis nemo debitis dolor.
                </p>
            </div>
        </li>
    )
}

export default ProductScreenshotsItem;