import React from 'react';

function BuyItNowItem() {
    return (
        <li className='buy-it-now__item'>
            <span className='buy-it-now__category'>Standart</span>
            <span>$100</span>
            <ol className='buy-it-now__list'>
                <li className='buy-it-now__list-item'>Porro officia cumque sint deleniti;</li>                <li className='buy-it-now__list-item'>Porro officia cumque sint deleniti;</li>
                <li className='buy-it-now__list-item'>Тemo facere rem vitae odit;</li>
                <li className='buy-it-now__list-item'>Cum odio, iste quia doloribus autem;</li>
                <li className='buy-it-now__list-item'>Aperiam nulla ea neque.</li>
            </ol>
        </li>
    )
}

export default BuyItNowItem;