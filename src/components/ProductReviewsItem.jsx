import React from 'react';

function ProductReviewsItem() {
    return (
        <li className='product-reviews__item'>
            <div className='product-reviews__image'></div>
            <div className='product-reviews__text-wrapper'>
                <p className='product-reviews__item-text'>
                    Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus officiis sapiente necessitatibus commodi consectetur?
                </p>
                <span>Lourens S.</span>
            </div>
        </li>
    )
}

export default ProductReviewsItem;