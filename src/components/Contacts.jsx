import React from 'react';

function Contacts() {
    return (
        <div className='contacts'>
            <div className='content-wrapper'>
            <h2 className='contacts-title title-h2'>Contacts</h2>
            <div className='contacts-form-wrapper'>
                <form className='contacts-form' action="">
                    <input type="text" placeholder='Your name:'/>
                    <input type="email" placeholder='Your email:'/>
                    <textarea placeholder='Your message:'></textarea>
                    <input type='submit' value='Send'/>
                </form>
            </div>
            <div className='contacts-form-info'>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <ul className='contacts-social-list'>
                    <li className='contacts-social-item'>
                        <a className='contacts-social-link'></a>
                    </li>
                    <li className='contacts-social-item'>
                        <a className='contacts-social-link'></a>
                    </li> <li className='contacts-social-item'>
                    <a className='contacts-social-link'></a>
                </li> <li className='contacts-social-item'>
                    <a className='contacts-social-link'></a>
                </li> <li className='contacts-social-item'>
                    <a className='contacts-social-link'></a>
                </li>
                </ul>
            </div>
            </div>
        </div>
    )
}

export default Contacts;